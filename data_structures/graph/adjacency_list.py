from collections import defaultdict


class Graph:

    def __init__(self):
        self.graph = defaultdict(list)

    def add_edge(self, u, v):
        self.graph[u].append(v)

    def bfs(self, v):
        visited = [False for _ in range(len(self.graph))]

        queue = [v]
        visited[v] = True

        while queue:
            v = queue.pop(0)
            print(v, end=" ")

            for i in self.graph[v]:
                if not visited[i]:
                    queue.append(i)
                    visited[i] = True

    def dfs_iter(self, v):
        visited = [False for _ in range(len(self.graph))]
        stack = [v]

        while stack:
            v = stack.pop()
            if not visited[v]:
                print(v, end=" ")
                visited[v] = True

            for i in self.graph[v]:
                if not visited[i]:
                    stack.append(i)

    def dfs_rec(self, v, visited):
        visited[v] = True
        print(v, end=" ")

        for i in self.graph[v]:
            if not visited[i]:
                self.dfs_rec(i, visited)

    def dfs(self, v):
        visited = [False] * len(self.graph)
        self.dfs_rec(v, visited)


if __name__ == '__main__':
    g = Graph()
    g.add_edge(0, 1)
    g.add_edge(0, 2)
    g.add_edge(1, 2)
    g.add_edge(2, 0)
    g.add_edge(2, 3)
    g.add_edge(3, 3)

    print(g.graph)

    g.dfs(2)
    print("\n")
    g.dfs_iter(2)

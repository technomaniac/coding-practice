vertex = 4
INF = 99999


def floyd_warshall(adjacency_matrix):
    for k in range(0, vertex):
        for i in range(0, vertex):
            for j in range(0, vertex):
                adjacency_matrix[i][j] = min(adjacency_matrix[i][j], adjacency_matrix[i][k] + adjacency_matrix[k][j])

    print("o/d", end='')
    for i in range(0, vertex):
        print("\t{:d}".format(i), end='')
    print()
    for i in range(0, vertex):
        print("{:d}".format(i), end='')
        for j in range(0, vertex):
            if adjacency_matrix[i][j] == INF:
                print("\tINF", end='')
            else:
                print("\t{:d}".format(adjacency_matrix[i][j]), end='')
        print()


graph = [
    [0, 5, INF, 10],
    [INF, 0, 3, INF],
    [INF, INF, 0, 1],
    [INF, INF, INF, 0]
]

floyd_warshall(graph)

visible_leafs = dict()


class Node:
    def __init__(self, root):
        self.root = root
        self.right = None
        self.left = None


def preorder(root, hd=0, depth=0):
    if root is None:
        return

    if hd not in visible_leafs.keys():
        visible_leafs[hd] = dict()
        visible_leafs[hd]['depth'] = depth
        visible_leafs[hd]['node'] = root.root
    else:
        if depth > visible_leafs[hd]['depth']:
            visible_leafs[hd]['node'] = root.root

    preorder(root.left, hd - 1, depth + 1)
    preorder(root.right, hd + 1, depth + 1)


if __name__ == '__main__':
    node = Node(2)
    node.left = Node(7)
    node.left.left = Node(2)
    node.left.right = Node(6)
    node.left.right.left = Node(5)
    node.left.right.right = Node(11)

    node.right = Node(5)
    node.right.right = Node(9)
    node.right.right.left = Node(4)
    node.right.right.left.left = Node(8)

    preorder(node)
    print(visible_leafs)

def solve(k, n):
    a = ["a"] * n
    x = k - n
    for i in reversed(range(n)):
        if x > 26:
            a[i] = 'z'
            x = (x - 26) + 1

        elif x == 26:
            a[i] = chr(ord(a[i]) + 25)
            x = x - 25

        elif x > 0:
            a[i] = chr(ord(a[i]) + x)
            x = 0

        if x == 0:
            break

    return ''.join(a)


# Write your code here

T = int(input())
for _ in range(T):
    n, k = map(int, input().split())

    out_ = solve(k, n)
    print(out_)

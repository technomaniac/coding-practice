# def base3(n):
#     r = n % 3
#     q = n / 3
#
#     if q == 0:
#         return ""
#     else:
#         return base3(int(q)) + str(int(r))
#
#
# def multiple_of_3(n):
#     q = n / 3
#     return base3(q - 1) + "3"


def getDigits(n):
    r = n % 3
    q = n / 3

    if q == 0:
        return ""
    elif r == 0:
        return getDigits(q - 1) + "3"
    else:
        return getDigits(int(q)) + str(int(r))


T = int(input())
for _ in range(T):
    n = int(input())

    out_ = len(getDigits(n))
    print(out_)

class Node:

    def __init__(self, key, value):
        self.key = key
        self.value = value
        self.next = None


class LinkedList:

    def __init__(self):
        self.head = None
        self.pairs = dict()

    def insert(self, k, v):
        if not self.pairs.get(k):
            node = Node(k, v)
            node.next = self.head
            self.head = node
            self.pairs[k] = v

    def is_exists(self, val):
        temp = self.head
        while temp:
            if temp.value == val:
                return True
            temp = temp.next
        return False

    def traverse(self):
        output_list = []
        temp = self.head
        while temp:
            output_list.append((temp.key, temp.value))
            temp = temp.next

        return reversed(output_list)


def solve(n, key, val):
    l = LinkedList()
    for i in range(len(key)):
        l.insert(key[i], val[i])

    return l.traverse()


# return a list with pair of strings and integers as per the output required

key = []
val = []
n = int(input())
for i in range(0, n):
    inp = input().split(' ')
    key.append(inp[0])
    val.append(int(inp[1]))

ans = (solve(n, key, val))
for i in ans:
    print(str(i[0]) + " " + str(i[1]))
